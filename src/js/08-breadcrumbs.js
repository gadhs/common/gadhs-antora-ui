;(function () {
  'use strict'

  function truncate (toolbar, breadcrumbs) {
    if (toolbar.scrollHeight === toolbar.getBoundingClientRect().height) return
    breadcrumbs = breadcrumbs.slice(1, -1).reverse()
    while (breadcrumbs.length && toolbar.scrollHeight > toolbar.getBoundingClientRect().height) {
      var it = breadcrumbs.pop()
      var text = (it = it.querySelector('a') || it).innerText.trim()
      var truncatedText = text.split(' ', 2)[0].slice(0, 8) + ' ...'
      it.setAttribute('title', text)
      it.innerText = truncatedText
    }
  }

  function onResize (toolbar, breadcrumbs) {
    breadcrumbs.forEach(function (breadcrumb) {
      if (!(breadcrumb = breadcrumb.querySelector('a') || breadcrumb).hasAttribute('title')) return
      breadcrumb.innerText = breadcrumb.getAttribute('title')
      breadcrumb.removeAttribute('title')
    })
    truncate(toolbar, breadcrumbs)
  }

  ;(function () {
    const toolbar = document.querySelector('.toolbar')
    const breadcrumbs = Array.prototype.slice.call(toolbar.querySelectorAll('.breadcrumbs li'))
    truncate(toolbar, breadcrumbs)
    window.addEventListener('resize', onResize.bind(null, toolbar, breadcrumbs))
  })()
})()
