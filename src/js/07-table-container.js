;(function () {
  'use strict'

  find(document.querySelector('article.doc'), 'table.tableblock').forEach(function (table) {
    var parent = table.parentNode
    var maxWidth = parent.getBoundingClientRect().width
    if (table.getBoundingClientRect().width <= maxWidth) return
    var container = Object.assign(document.createElement('div'), { className: 'tablecontainer overflow' })
    table.parentNode.insertBefore(container, table)
    container.appendChild(table)
  })

  function find (from, selector) {
    return [].slice.call(from.querySelectorAll(selector))
  }
})()
